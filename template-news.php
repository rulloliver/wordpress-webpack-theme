<?php 
/* Template name: News */
get_header(); ?>

<div id="primary" class="content-area">
    <div class="container">
        <div class="columns news-layout--title">
            <div class="column is-5">
                <div class="news-title" data-aos="fade-up">
                    <h2>Uudised</h2>
                </div>
            </div>
        </div>
        <div class="columns">
            <div class="column news-section news-layout">
                <div class="news--inner" data-aos="fade-up" data-aos-delay="200">

                    <?php 
                    $args = array( 'post_type' => 'news', 'posts_per_page' => -1 );
                    $the_query = new WP_Query( $args ); 
                    ?>
                    <?php if ( $the_query->have_posts() ) : ?>
                
                    <div class="single-news--wrap">
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <div class="single-news">
                            <div class="single-news--inner">
                                <div class="single-news--img">
                                    <a href="<?php the_permalink() ?>"><div class="news-thumb" style="background:url('<?php the_post_thumbnail_url( 'medium' ); ?>');"></div></a>
                                </div>
                                <div class="single-news--meta">
                                    <span><?php echo get_the_date(); ?></span>
                                    <a href="<?php the_permalink() ?>"><h4><?php the_title(); ?></h4></a>
                                    <a href="<?php the_permalink() ?>" class="btn minimal">Loe edasi<i></i></a>
                                </div>
                            </div>
                        </div>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </div>
                    <?php endif; ?>
                 
                </div>
            </div>
        </div>
    </div>
</div><!-- .content-area -->

<?php get_footer(); ?>