<div class="heroslider--wrap">
    <div id="hero-slider" class="heroslider swiper">
        <!-- If we need navigation buttons -->
        <div class="swiper-button-prev hero-button-prev"></div>
        <div class="swiper-button-next hero-button-next"></div>
        <!-- If we need scrollbar -->
        <div class="swiper-scrollbar"></div>
        <div class="swiper-wrapper">
            <?php if(have_rows('add-slides')): ?>
                <?php while(have_rows('add-slides')) : the_row(); ?>
                <div class="hero-slide swiper-slide">
                    <div class="hero-slide--img" style="background:url('<?php the_sub_field('bg'); ?>') no-repeat;"></div>
                    <div class="hero-slide--cut"></div>
                    <div class="hero-slide--content">
                        <div class="container">
                            <div class="columns">
                                <div class="column">
                                    <div class="single-slide columns">
                                        <div class="single-slide--textarea column is-offset-1 is-5" data-aos="fade-up">
                                            <?php the_sub_field('textarea'); ?>

                                            <?php if(get_sub_field('h-activate')): ?>
                                                <div class="highlight">
                                                    <?php if(get_sub_field('highlight-description')): ?>
                                                        <p><?php the_sub_field('highlight-description'); ?></p>
                                                    <?php endif; ?>
                                                    <?php if(get_sub_field('highlight-price')): ?>
                                                        <h3><?php the_sub_field('highlight-price'); ?></h3>
                                                    <?php endif; ?>
                                                </div>
                                            <?php endif; ?>

                                            <?php
                                                $link = get_sub_field('button');
                                                if( $link ):
                                                    $link_url = $link['url'];
                                                    $link_title = $link['title'];
                                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                            ?>
                                            <a href="<?php echo $link_url; ?>" class="btn" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?><i></i></a>
                                            <?php endif; ?>
                                        </div>
                                        <div class="single-slide--img column is-paddingless">
                                            <?php 
                                                $image = get_sub_field('image');
                                                $size = 'full'; // (thumbnail, medium, large, full or custom size)
                                                if( $image ) {
                                                    echo wp_get_attachment_image( $image, $size, "", array( "class" => "screen-img" ) );
                                            } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php if(have_rows('add-icons')): ?>
<div class="hero-certificates">
    <div class="container">
        <div class="columns">
            <div class="column hero-certificates--inner" data-aos="fade-up" data-aos-delay="200">
                <?php while(have_rows('add-icons')) : the_row(); ?>
                    <?php 
                        $image = get_sub_field('icon');
                        $size = 'medium'; // (thumbnail, medium, large, full or custom size)
                        if( $image ) {
                            echo wp_get_attachment_image( $image, $size, "", array( "class" => "icon-img" ) );
                    } ?>
                <?php endwhile; ?>
            </div>
        </div>  
    </div>
</div>
<?php endif; ?>