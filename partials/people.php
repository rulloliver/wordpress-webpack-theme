<div class="people">
    <div class="container">
        <div class="columns">
            <div class="column people--title has-text-centered" data-aos="fade-up">
                <h2><?php the_field('people-title','option'); ?></h2>
            </div>
        </div>
        <div class="columns">
            <div class="column">
                <?php if(have_rows('people','option') ) : ?>
                <div class="people--wrap">
                    <?php $counter=1; while(have_rows('people','option') ) : the_row(); ?>
                        <div class="single-person" data-aos="fade-up" data-aos-delay="<?php echo $counter;?>00">
                            <div class="single-person--inner">
                                <div class="single-person--img" style="background:url('<?php the_sub_field('image','option'); ?>');"></div>
                                <div class="single-person--content">
                                    <?php the_sub_field('info','option'); ?>
                                </div>
                            </div>
                        </div>
                    <?php $counter++; endwhile; ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
