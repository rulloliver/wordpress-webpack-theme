  <!-- FAQ -->
  <div id="faq" class="b-faq" data-aos="fade-up">
    <div class="container">
      <div class="columns is-centered">
        <div class="column is-8">

          <div data-aos="fade-up" data-aos-delay="100"> 
            <?php if( have_rows('questions')): ?>
              <?php while (have_rows('questions')): the_row(); ?>
                <div class="b-faq-item">
                  <input type="radio" id="radio-<?php echo get_row_index(); ?>" name="faq-items">
                  <label for="radio-<?php echo get_row_index(); ?>" class="b-faq-question"><?php the_sub_field('questions'); ?></label>
                  <div class="b-faq-answer">
                    <p><?php the_sub_field('answer'); ?></p>
                  </div>
                </div>
              <?php endwhile; ?>
            <?php endif; ?>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- END FAQ -->