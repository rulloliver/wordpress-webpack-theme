
<div id="p-slider" class="p-slider">
    <div class="container">
        <div class="columns is-centered">
            <div class="column is-8 p-slider--title has-text-centered" data-aos="fade-up">
                <h2><?php the_sub_field('title'); ?></h2>
            </div>
        </div>
        <div class="columns">
            <div class="column p-slider--inner swiper-container" data-aos="fade-up" data-aos-delay="200">
                <?php the_sub_field('shortcode'); ?>
            </div>
        </div>  
        <div class="columns">
            <div class="column has-text-centered news-recent--cta" data-aos="fade-up" data-aos-delay="400">
                <a href="/tooted" class="btn">Kõik tooted<i></i></a>
            </div>
        </div>
    </div>
</div>
