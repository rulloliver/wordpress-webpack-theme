<div class="numblocks">
    <div class="container">

        <div class="columns is-centered">
            <div class="column is-8 numblocks--title has-text-centered" data-aos="fade-up">
                <h2><?php the_sub_field('title'); ?></h2>
            </div>
        </div>
        <?php if(have_rows('blocks')) : ?>
        <div class="columns is-centered">
            <div class="column is-10">
                <div class="numblocks--wrap">
                    <?php $counter = 1; while(have_rows('blocks') ) : the_row(); ?>
                        <div class="single-numblock" data-aos="fade-up" data-aos-delay="<?php echo $counter; ?>00">
                            <div class="single-numblock--inner is-flex">
                                <div class="num">
                                    <span>0<?php echo $counter; ?></span>
                                </div>
                                <div class="single-numblock--content">
                                    <h3><?php the_sub_field('text'); ?></h3>
                                </div>
                            </div>
                        </div>
                    <?php $counter++; endwhile; ?>
                </div>
            </div>
        </div>
        <?php endif; ?>

    </div>
</div>