<div class="contact-form">
    <div class="container">
        <div class="columns is-centered">
            <div class="column is-11">
                <div class="contact-form--inner" data-aos="fade-up">
                    <?php echo do_shortcode('[gravityform id="1" title="true" description="false" ajax="true" tabindex="49"]'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
