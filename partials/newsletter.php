<?php the_field('n-title','option'); ?>
<form id="smly" action="https://sa375wli.sendsmaily.net/api/opt-in/" method="post">
	<p class="error" style="padding:15px;background-color:#f2dede;margin:0 0 10px;display:none"></p>
	<p class="success" style="padding:15px;background-color:#dff0d8;margin:0 0 10px;display:none">Täname, et liitusite meie uudiskirjaga.</p>
	<input type="hidden" name="lang" value="et" />
	<input type="hidden" name="success_url" value="<?php the_field('n-success','option'); ?>" />
	<input type="hidden" name="failure_url" value="<?php the_field('n-fail','option'); ?>" />
	<p><input type="email" name="email" value="" placeholder="E-post" required/></p>
		<p><button type="submit" class="button white">Liitu<i></i></button></p>
</form>
