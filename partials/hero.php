<div class="heroarea">
<div class="heroarea--img" style="background:url('<?php the_sub_field('bg'); ?>') no-repeat;"></div>
<div class="heroarea--cut"></div>

<div class="heroarea--content">
    <div class="container">
        <div class="columns">
            <div class="column columns">
                <div class="column is-6" data-aos="fade-up">
                    <?php the_sub_field('content'); ?>
                </div>
            </div>
        </div>
    </div>
</div> 
</div>