<div class="three-columns<?php if(get_sub_field('last-block')): ?> last-block<?php endif; ?>">
    <div class="container">

        <div class="columns is-centered">
            <div class="column is-8 three-columns--title has-text-centered" data-aos="fade-up">
                <?php the_sub_field('title'); ?>
            </div>
        </div>

        <div class="columns is-centered">
            <div class="column is-10">
                <?php if(have_rows('columns')) : ?>
                <div class="columns">
                    <?php $counter=1; while(have_rows('columns') ) : the_row(); ?>
                    
                        <div class="three-columns--box column is-4 has-text-centered">
                            <div class="three-columns--box-inner" data-aos="fade-up" data-aos-delay="<?php echo $counter; ?>00">
                                <?php 
                                    $image = get_sub_field('icon');
                                    $size = 'full'; // (thumbnail, medium, large, full or custom size)
                                    if( $image ) {
                                        echo wp_get_attachment_image( $image, $size, "", array( "class" => "icon-img" ) );
                                } ?>
                                <?php the_sub_field('textarea'); ?>
                            </div>
                        </div>

                    <?php $counter++; endwhile; ?>
                </div>
                <?php endif; ?>
            </div>
        </div>

    </div>
</div>