
<div class="videoplayer">
    <div class="container-wide">
        <div class="columns">
            <div class="column videoplayer--inner" data-aos="fade-up">
                <?php 
                    $image = get_sub_field('image');
                    $size = 'full'; // (thumbnail, medium, large, full or custom size)
                    if( $image ) {
                        echo wp_get_attachment_image( $image, $size, "", array( "class" => "videoplayer-img" ) );
                } ?>
                <?php if(get_sub_field('youtube')) : ?><a class="button-play video-play" data-toggle="modal" data-src="<?php the_sub_field('youtube'); ?>" data-target="#modal"><img src="<?php bloginfo('template_directory'); ?>/assets/icon--play.svg" width="111" height="111" alt="Efeco video play"></a><?php endif; ?>
            </div>
        </div>  
    </div>
</div>
<?php if(get_sub_field('youtube')) : ?>
<div id="modal" class="modal">
    <div class="modal-background"></div>
    <div class="modal-content">
   <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="" id="video-modal"  allowscriptaccess="always" allow="autoplay"></iframe> 
        </div> 
    </div>
    <!-- <a class="close-modal">Close</a> -->
</div>
<?php endif; ?>
