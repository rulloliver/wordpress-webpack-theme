<div class="contact-details">
    <div class="container">

        <div class="columns is-centered">
            <div class="column is-8 contact-details--title has-text-centered" data-aos="fade-up">
                <h2><?php the_sub_field('title'); ?></h2>
            </div>
        </div>
        
        <div class="columns is-centered">
            <div class="column is-6 columns contact-details--content">
                <div class="column" data-aos="fade-up">
                    <?php the_sub_field('left'); ?>
                </div>
                <div class="column" data-aos="fade-up" data-aos-delay="200">
                    <?php the_sub_field('right'); ?>
                </div>
            </div>
        </div>
     

    </div>
</div>