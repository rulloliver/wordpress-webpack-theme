
<div class="banner">
    <div class="container">
        <div class="columns">
            <div class="column banner--inner" data-aos="fade-up" data-aos-delay="200">
                <?php
                $link = get_sub_field('button');
                if( $link ):
                    $link_url = $link['url'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                ?>
                <a href="<?php echo $link_url; ?>" target="<?php echo esc_attr($link_target); ?>">
                <?php endif; ?>
                    <?php 
                        $image = get_sub_field('image');
                        $size = 'full'; // (thumbnail, medium, large, full or custom size)
                        if( $image ) {
                            echo wp_get_attachment_image( $image, $size, "", array( "class" => "is-hidden-mobile banner-img" ) );
                    } ?>
                    <?php 
                        $image = get_sub_field('image-mobile');
                        $size = 'full'; // (thumbnail, medium, large, full or custom size)
                        if( $image ) {
                            echo wp_get_attachment_image( $image, $size, "", array( "class" => "is-hidden-desktop banner-img" ) );
                    } ?>
                <?php if($link): ?>
                </a>
                <?php endif; ?>
            </div>
        </div>  
    </div>
</div>
