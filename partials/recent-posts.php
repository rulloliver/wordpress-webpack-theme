<div id="recent-posts" class="blog blog-recent">
    <div class="container">
        <div class="columns is-centered">
            <div class="column is-5">
                <div class="blog-title has-text-centered" data-aos="fade-up">
                    <h2>Blogi</h2>
                </div>
            </div>
        </div>
        <div class="columns">
            <div class="column blog-swiper swiper-container">
                <?php 
            $args = array( 
                'posts_per_page' => 4,
                'post__not_in' => array( $post->ID )
            );
            $the_query = new WP_Query( $args ); 
            ?>
            <?php if ( $the_query->have_posts() ) : ?>
                <div class="blog-single--wrap swiper-wrapper" data-aos="fade-up" data-aos-delay="200">
                    <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
                    <div class="blog-single swiper-slide">
                        <div class="blog-single--inner">
                            <a href="<?php the_permalink() ?>"><div class="blog-thumb" style="background:url('<?php the_post_thumbnail_url( 'medium' ); ?>');"></div></a>
                            <a href="<?php the_permalink() ?>"><h4><?php the_title(); ?></h4></a>
                            <?php the_excerpt(); ?>
                            <a href="<?php the_permalink() ?>" class="btn minimal">Loe edasi<i></i></a>
                        </div>
                    </div>
                    <?php endwhile; wp_reset_postdata(); ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
        <div class="columns">
            <div class="column has-text-centered blog-recent--cta" data-aos="fade-up">
                <a href="/blogi" class="btn">Kõik blogipostitused<i></i></a>
            </div>
        </div>
    </div>
</div>  <!-- recent blog items -->