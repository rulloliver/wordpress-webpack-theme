<div id="news-slider" class="news-section news-recent">
    <div class="container">
        <div class="columns is-centered">
            <div class="column is-5">
                <div class="news-title has-text-centered" data-aos="fade-up">
                    <h2>Uudised</h2>
                </div>
            </div>
        </div>
        <div class="news--inner" data-aos="fade-up" data-aos-delay="200">
            <?php 
            $args = array( 
                'post_type' => 'news', 
                'posts_per_page' => 10,
                'post__not_in' => array( $post->ID )
            );
            $the_query = new WP_Query( $args ); 
            ?>
            <?php if ( $the_query->have_posts() ) : ?>
            <div>
            <div class="single-news--wrap swiper-container">
                <div class="swiper-wrapper">
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <div class="single-news swiper-slide">
                        <div class="single-news--inner">
                            <div class="single-news--img">
                                <a href="<?php the_permalink() ?>"><div class="news-thumb" style="background:url('<?php the_post_thumbnail_url( 'medium' ); ?>');"></div></a>
                            </div>
                            <div class="single-news--meta">
                                <span><?php echo get_the_date(); ?></span>
                                <a href="<?php the_permalink() ?>"><h4><?php the_title(); ?></h4></a>
                                <a href="<?php the_permalink() ?>" class="btn minimal">Loe edasi<i></i></a>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; wp_reset_postdata(); ?>
                </div>
            </div>
            </div>
            <?php endif; ?>
            <div class="news--nav">
                <!-- If we need navigation buttons -->
                <div class="news-button-prev swiper-button-prev"></div>
                <div class="news-button-next swiper-button-next"></div>
            </div>
        </div>
        <div class="columns">
            <div class="column has-text-centered news-recent--cta" data-aos="fade-up">
                <a href="/uudised" class="btn all-btn">Kõik uudised<i></i></a>
            </div>
        </div>
    </div>
</div>  <!-- recent news items -->