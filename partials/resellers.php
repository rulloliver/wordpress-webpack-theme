<?php if(have_rows('resellers','option')) : ?>
<div class="resellers is-flex">
    <?php $counter = 1; while(have_rows('resellers','option')) : the_row(); ?>
        <div class="single-reseller" data-aos="fade-up" data-aos-delay="<?php echo $counter;?>00">
            <div class="single-reseller--inner">
                <?php 
                    $image = get_sub_field('logo','option');
                    $size = 'medium'; // (thumbnail, medium, large, full or custom size)
                    if( $image ) {
                        echo wp_get_attachment_image( $image, $size, "", array( "class" => "reseller-img" ) );
                } ?>
                <?php
                    $link = get_sub_field('link','option');
                    if( $link ):
                        $link_url = $link['url'];
                        $link_title = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                ?>
                <a href="<?php echo $link_url; ?>" class="btn minimal" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?><i></i></a>
                <?php endif; ?>

            </div>
        </div>
    <?php $counter++; endwhile; ?>
</div>
<?php endif; ?>