
<div class="regular-content">
    <div class="container">
        <div class="columns is-centered">
            <div class="column is-7 regular-content--inner">
                <article data-aos="fade-up">
                    <?php the_sub_field('content'); ?>
                </article>
            </div>
        </div>  
    </div>
</div>
