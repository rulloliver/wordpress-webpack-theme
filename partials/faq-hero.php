<div class="heroarea">
    <div class="heroarea--img" style="background:url('<?php the_post_thumbnail_url(); ?>') no-repeat;"></div>
    <div class="heroarea--cut"></div>

    <div class="heroarea--content">
        <div class="container">
            <div class="columns">
                <div class="column columns">
                    <div class="column is-6" data-aos="fade-up">
                        <?php if (have_posts()) : ?>
                        <?php while (have_posts()) : the_post(); ?>
                            <article>
                                <h1><?php the_title(); ?></h1>
                                <?php the_content(); ?>
                            </article>
                        <?php endwhile; endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>

<?php get_template_part('partials/faq'); ?>
