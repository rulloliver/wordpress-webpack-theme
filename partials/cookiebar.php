
<?php if( get_field('cookie-message','option') ): ?>
<!-- cookie policy -->
<div class="cookie-message">
  <div class="cookie-message--inner is-flex">
      <div class="cookie-content">
      <p><?php the_field('cookie-message','option'); ?>
      <?php
        $link = get_field('link','option');
        if( $link ):
        $link_url = $link['url'];
        $link_title = $link['title'];
        $link_target = $link['target'] ? $link['target'] : '_self'; ?>
        <a href="<?php echo $link_url; ?>" class="location-link" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?>.</a><?php endif; ?></p>
    </div>
    <div class="cookie-btn">
        <a class="cookie-close btn border-button"><?php echo __('Nõustun','efeco-theme'); ?></a>
    </div>
  </div>
</div>
<!-- end cookie -->
<?php endif; ?>