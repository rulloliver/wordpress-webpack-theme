<div class="three-columns">
    <div class="container">

        <div class="columns is-centered">
            <div class="column is-8 three-columns--title has-text-centered" data-aos="fade-up">
                <?php the_sub_field('title'); ?>
            </div>
        </div>

        <div class="columns is-centered">
            <div class="column is-11">
                <div class="resellersblock">
                   <?php get_template_part('partials/resellers'); ?>
                </div>
            </div>
        </div>

    </div>
</div>