
<footer id="footer-content">
    <div class="footer-cut"></div>
    <div class="footer-top">
        <div class="container footer-container">
            <div class="columns">
                <div class="column footer-menu is-narrow">
                    <div class="footer-menu--inner">
                        <?php wp_nav_menu( array('menu' => 'footer-1') ); ?>
                    </div>
                </div>
                <div class="column footer-menu is-narrow">
                    <div class="footer-menu--inner">
                        <?php wp_nav_menu( array('menu' => 'footer-2') ); ?>
                    </div>
                </div>
                <div class="column footer-newsletter is-narrow">
                    <div class="footer-menu--inner">
                        <?php get_template_part('partials/newsletter'); ?> 
                    </div>
                </div>
                <div class="column"></div>
            </div>
        </div>
    </div><!-- top -->
    <div class="footer-bottom">
        <div class="container">
            <div class="columns">
                <div class="column footer-bottom--social is-flex is-narrow">
                    <?php if(get_field('facebook','option') ) : ?>
                        <a class="social-icon" href="<?php the_field('facebook','option'); ?>">
                            <span class="social-icon--holder fb"></span>
                        </a>
                    <?php endif; ?>
                    <?php if(get_field('instagram','option') ) : ?>
                        <a class="social-icon" href="<?php the_field('instagram','option'); ?>">
                            <span class="social-icon--holder ig"></span>
                        </a>
                    <?php endif; ?>
                    <?php if(get_field('youtube','option') ) : ?>
                        <a class="social-icon" href="<?php the_field('youtube','option'); ?>">
                            <span class="social-icon--holder yt"></span>
                        </a>
                    <?php endif; ?>
                </div>
                <div class="column footer-bottom--textarea">
                    <?php the_field('footer-info','option'); ?>
                </div>
                <div class="column footer-bottom--logos is-flex is-narrow">
                    <?php if(have_rows('logos','option')): ?>
                        <?php while(have_rows('logos','option') ) : the_row(); ?>
                            <?php 
                                $image = get_sub_field('logo','option');
                                $size = 'medium'; // (thumbnail, medium, large, full or custom size)
                                if( $image ) {
                                    echo wp_get_attachment_image( $image, $size, "", array( "class" => "logo-img" ) );
                            } ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php get_template_part('partials/cookiebar'); ?> 

    <?php wp_footer(); ?>
    </body>
</html>