<?php
/**
 * The Sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<div id="secondary">

	<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">

	</div><!-- #primary-sidebar -->

</div><!-- #secondary -->