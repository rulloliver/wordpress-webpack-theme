<?php get_header(); ?>

<div id="primary" class="content-area">
    <div class="container">
        <div class="columns is-centered">
            <div class="column is-7">
                <div class="single-content">
                    <div class="blog-single--hero" data-aos="fade-up">
                        <h1><?php the_title(); ?></h1>
                        <div class="columns blog-single--meta">
                            <div class="column">
                                <span class="post-date"><?php echo get_the_date(); ?></span>
                            </div>
                            <span class="column is-narrow">
                                <div class="social-share is-flex">
                                    <a class="social-share-link" href="https://facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" rel="noopener" aria-label="Share on Facebook"><img class="social-share--img" src="<?php bloginfo('template_url'); ?>/assets/icon--fb-dark.svg" alt="Facebook icon"></a>
                                    <!-- <a class="social-share-link" href="https://twitter.com/intent/tweet/?text=<?php the_title(); ?>&amp;url=<?php the_permalink(); ?>" target="_blank" rel="noopener" aria-label="Share on Twitter"><img class="social-share--img" src="<?php bloginfo('template_url'); ?>/assets/icon--twitter-post.svg" alt="Twitter icon"></a> -->
                                    <a class="social-share-link" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>&amp;title=<?php the_title(); ?>&amp;summary=<?php the_title(); ?>&amp;source=<?php the_permalink(); ?>" target="_blank" rel="noopener" aria-label="Share on LinkedIn"><img class="social-share--img" src="<?php bloginfo('template_url'); ?>/assets/icon--linkedin.svg" alt="Linkedin icon"></a>
                                    <a class="social-share-link copy-link" href="#" rel="noopener"><img class="social-share--img" src="<?php bloginfo('template_url'); ?>/assets/icon--link.svg" alt="Copy icon"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if (have_posts()) : ?>
                        <?php while (have_posts()) : the_post(); ?>
                            <article  data-aos="fade-up">
                                <?php the_content(); ?>
                            </article>
                    <?php endwhile; endif; ?>
                </div>
            </div>
        </div>
    </div>

     <?php get_template_part('partials/recent','news'); ?>
     
</div><!-- .content-area -->

<?php get_footer(); ?>