<?php 
/* Template name: Blog */
get_header(); ?>

<div id="primary" class="content-area">

    <div class="blog blog-all">
        <div class="container">
            
            <div class="columns">
                <div class="column is-5">
                    <div class="blog-title" data-aos="fade-up">
                        <h2>Blogi</h2>
                    </div>
                </div>
            </div>

            <div class="columns">
                <div class="column">
                        
                        <?php $the_query = new WP_Query( 'posts_per_page=-1' ); ?>
                        <div class="blog-single--wrap" data-aos="fade-up" data-aos-delay="200">

                            <div class="masonry-grid">
                                <div class="grid-sizer"></div>
                                <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
                                <div class="grid-item blog-single">
                                    <div class="blog-single--inner">
                                        <a href="<?php the_permalink() ?>"><div class="blog-thumb" style="background:url('<?php the_post_thumbnail_url( 'medium' ); ?>');"></div></a>
                                        <a href="<?php the_permalink() ?>"><h4><?php the_title(); ?></h4></a>
                                        <?php the_excerpt(); ?>
                                        <a href="<?php the_permalink() ?>" class="btn minimal">Loe edasi<i></i></a>
                                    </div>
                                </div>
                                <?php endwhile; wp_reset_postdata(); ?>
                            </div>

                    </div>
                </div>
            </div>
            
        </div>
    </div>  <!-- recent blog items -->

</div><!-- .content-area -->
<script src="<?php bloginfo('template_url'); ?>/javascripts/masonry.pkgd.min.js"></script>
<script>
jQuery('.masonry-grid').masonry({
  // options
  itemSelector: '.grid-item',
});
</script>
<?php get_footer(); ?>