<?php get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
    <?php  if ( is_product() ){ ?>
    <div class="heroarea">
        <div class="heroarea--img" style="background:url('<?php if(get_field('cover-photo')): ?><?php the_field('cover-photo'); ?><?php else: ?><?php bloginfo('template_url'); ?>/assets/img--Toode_oranz.jpg<?php endif; ?>') no-repeat;"></div>
        <div class="heroarea--cut"></div>

        <div class="heroarea--content">
            <div class="container">
                <div class="columns">
                    <div class="column columns">
                        <div class="column is-11 is-offset-1">
                            <?php woocommerce_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
    <?php   }else{  ?>
        <div class="default-content">
            <div class="default-content--content">
                <div class="container">
                    <div class="columns">
                        <div class="column" data-aos="fade-up">
                            <?php woocommerce_content(); ?>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    <?php   }   ?>
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>