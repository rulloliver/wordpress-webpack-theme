import $ from 'jquery'
import Swiper from '../../../node_modules/swiper/swiper-bundle.js'
import '../../../node_modules/swiper/swiper-bundle.min.css'
import '../../../javascripts/jquery.cookieBar.min'

import AOS from 'aos'
import 'aos/dist/aos.css'
var Clipboard = require('clipboard')

export default {
  init () {
    // JavaScript to be fired on all pages
    /* Fade in
================================================ */
    AOS.init({
      duration: 1500, // values from 0 to 3000, with step 50ms
      once: true,

      disable: function () {
        var maxWidth = 769
        return window.innerWidth < maxWidth
      }
    })
    /* SEARCH
================================================ */
    // search functionality
    $('#search-btn span').click(function () {
      $('#search-btn').toggleClass('grow')
      $('#search-btn').parent().children('.search-wrap').toggleClass('active')
      $('body').toggleClass('overlay-body overlay-search')
      $('#toggle').removeClass('active')
      $('#overlay').removeClass('open')
      $('header').toggleClass('active-overlay')
      $('.search-live-field').val('')
    })
    $('.grow span').click(function () {
      $('.menu > .menu-item-has-children > a').parent().children('.sub-menu-wrap').removeClass('active')
      $('.menu > .menu-item-has-children > a').parent().removeClass('active')
      $('body').removeClass('overlay-body')
      $('body').removeClass('overlay-body overlay-search')
    })
    /* FAQ
================================================ */
    if (document.getElementById('faq')) {
      $('.b-faq .b-faq-question').on('click', function (event) {
        if ($(this).parents('.b-faq-item').first().hasClass('active')) {
          $('.b-faq-item').removeClass('active')
          $('input[name="faq-items"]').prop('checked', false)
          event.preventDefault()
        } else {
          $('.b-faq-item').removeClass('active')
          $(this).parents('.b-faq-item').first().addClass('active')
        }
      })
    }
    /* COPY TO CLIPBOARD
================================================ */
    var clipboard = new Clipboard('.copy-link', {
      text: function () {
        return window.location.href
      }
    })
    clipboard.on('success', function (e) {
      console.info('Action:', e.action)
      console.info('Text:', e.text)
      console.info('Trigger:', e.trigger)
      window.alert('Copied results for: ' + e.text)
      e.clearSelection()
    })
    var clipboardUrl = new Clipboard('.demo-permalink', {

    })
    clipboardUrl.on('success', function (e) {
      console.info('Action:', e.action)
      console.info('Text:', e.text)
      console.info('Trigger:', e.trigger)
      window.alert('Copied results for: ' + e.text)
      e.clearSelection()
    })
    /* Product slider
================================================ */
    function productProperties () {
      return new Swiper('.p-slider--inner .woocommerce', {
        autoHeight: true, // enable auto height
        speed: 500,
        slideClass: 'product',
        wrapperClass: 'products',
        spaceBetween: 8,
        effect: 'slide',
        fadeEffect: {
          crossFade: true,
        },
        breakpoints: {
          0: {
            slidesPerView: 1.2,
          },
          769: {
            slidesPerView: 3,
          }
        }
        // Responsive breakpoints
      })
    }
    // if has logomarquee then run script
    if (document.getElementById('p-slider')) {
      // Here we define a variable that returns the swiper
      const carousel = productProperties()
      // Afte we define this variable we can finally call the init function
      carousel.init()
    }
    /* Single woocommerce slider
================================================ */
    function productSingleProperties () {
      return new Swiper('.related-wrap', {
        autoHeight: true, // enable auto height
        slidesPerView: 4,
        speed: 500,
        slideClass: 'product',
        wrapperClass: 'products',
        spaceBetween: 8,
        effect: 'slide',
        fadeEffect: {
          crossFade: true,
        },
        breakpoints: {
          0: {
            slidesPerView: 1.2,
            spaceBetween: 8,
          },
          769: {
            slidesPerView: 4,
            spaceBetween: 0,
          }
        }
      })
    }
    // if has logomarquee then run script
    if (document.getElementById('related-products')) {
      // Here we define a variable that returns the swiper
      const carousel = productSingleProperties()
      // Afte we define this variable we can finally call the init function
      carousel.init()
    }
    /* Hero slider if component exists
================================================ */

    function carouselProperties () {
      return new Swiper('.swiper', {
        autoHeight: true, // enable auto height
        slidesPerView: 1,
        navigation: {
          nextEl: '.hero-button-next',
          prevEl: '.hero-button-prev',
        },
        speed: 500,

        autoplay: {
          delay: 5000,
        },
        scrollbar: {
          el: '.swiper-scrollbar',
          snapOnRelease: true,
          draggable: true,
        },
        effect: 'fade',
        breakpoints: {
          768: {
            slidesPerView: 1,
          }
        }
        // Responsive breakpoints
      })
    }
    // if has logomarquee then run script
    if (document.getElementById('hero-slider')) {
      // Here we define a variable that returns the swiper
      const carousel = carouselProperties()
      // Afte we define this variable we can finally call the init function
      carousel.init()
    }
    /*
Scrolling adds menu fixed class
=============================================== */
    // fixed class scroll down
    $(window).scroll(function () {
      if ($(window).scrollTop() > 0) {
        $('.site-header').addClass('fixed')
      } else {
        $('.site-header').removeClass('fixed')
      }
    })
    /* news slider if exists
================================================ */
    function blogcarouselProperties () {
      return new Swiper('.blog-swiper', {
        slidesPerView: 4,
        speed: 1000,
        breakpoints: {
          0: {
            slidesPerView: 1.25,
            spaceBetween: 8,
            autoHeight: false,
          },
          769: {
            slidesPerView: 4,
          }
        }
        // Responsive breakpoints
      })
    }
    // if has logomarquee then run script
    if (document.getElementById('recent-posts')) {
      // Here we define a variable that returns the swiper
      const carouselNews = blogcarouselProperties()
      // Afte we define this variable we can finally call the init function
      carouselNews.init()
    }
    /* news slider if exists
================================================ */
    function newscarouselProperties () {
      return new Swiper('.single-news--wrap', {
        slidesPerView: 1.75,
        centeredSlides: true,
        speed: 1000,

        navigation: {
          nextEl: '.news-button-next',
          prevEl: '.news-button-prev',
        },
        breakpoints: {
          0: {
            centeredSlides: false,
            spaceBetween: 8,
            slidesPerView: 1.25,
            autoHeight: true,

          },
          769: {
            centeredSlides: true,
            slidesPerView: 1,
          }
        }
        // Responsive breakpoints
      })
    }
    // if has logomarquee then run script
    if (document.getElementById('news-slider')) {
      // Here we define a variable that returns the swiper
      const carouselNews = newscarouselProperties()
      // Afte we define this variable we can finally call the init function
      carouselNews.init()
    }
    /* COOKIE POLICY
================================================ */
    $('.cookie-message').cookieBar({
      closeButton: '.cookie-close',
      hideOnClose: false,
      secure: false,
    })
    $('.cookie-message').on('cookieBar-close', function () {
      $(this).slideUp()
    })
    /* MOBILE MENU
================================================ */
    $('#toggle').click(function () {
      $(this).toggleClass('active')
      $('#overlay').toggleClass('open')
      $('#masthead').toggleClass('overlay-open')
    })
    /* MOBILE SUBMENU
================================================ */
    const $container = $('.mobile-menu')
    $container.find('.sub-menu').hide()
    $('.mobile-menu .menu-item-has-children').click(function () {
      $(this).toggleClass('active')
      $(this).children('.sub-menu').toggle()
      $('.mobile-menu .menu-item-has-children')
        .not(this)
        .removeClass('active')
        .children('.sub-menu')
        .hide()
    })
    /* ANCHORING
================================================ */
    $('a[href^="#"]').click(function (e) {
      e.preventDefault()
    })
    // SMOOTH SCROLL BUTTONS
    $(document).on('click', 'a[href^="#"]', function (e) {
      e.preventDefault()
      var target = this.hash
      var $target = $(target)
      $('html, body')
        .stop()
        .animate(
          {
            scrollTop: $target.offset().top - 0,
          },
          1000
        )
    })
    /* STICKY JS
================================================ */
    // Sticky jS
    /* Video modal
================================================ */
    var $videoSrc
    $('.video-play').click(function (e) {
      console.log('btn')
      e.preventDefault()
      $videoSrc = $(this).data('src')
    })

    $('.video-play').click(function (e) {
      console.log('modal')
      e.preventDefault()
      $('.modal').addClass('is-active')
      $('#video-modal').attr(
        'src',
        $videoSrc + '?autoplay=1&amp;modestbranding=1&amp;showinfo=0'
      )
    })
    // stop playing the youtube video when I close the modal
    $('.close-modal, .modal-background').click(function () {
      $('.modal').removeClass('is-active')
      $('#video-modal').attr('src', $videoSrc)
    })
  },
  finalize () {
    // JavaScript to be fired on all pages, after page specific JS is fired
    /* eslint-disable */

  },
}
