<?php get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <div class="default-content">
            <div class="default-content--img" style="background:url('<?php the_post_thumbnail_url(); ?>') no-repeat;"></div>
            <div class="default-content--cut"></div>

            <div class="default-content--content">
                <div class="container">
                    <div class="columns">
                        <div class="column columns">
                            <div class="column is-6">
                                <?php if (have_posts()) : ?>
                                    <?php while (have_posts()) : the_post(); ?>
                                        <article>
                                            <h1><?php the_title(); ?></h1>
                                            <?php the_content(); ?>
                                        </article>
                                <?php endwhile; endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>

    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>