<?php 
// REMOVE ADD TO CART 
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 20);
// REMOVE SORTING
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
// REMOVE SHOWING SINGLE RESULT
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );

/* For StoreFront Theme */
add_action( 'init', 'bbloomer_delay_remove_result_count' );
 
function bbloomer_delay_remove_result_count() {
   remove_action( 'woocommerce_after_shop_loop', 'woocommerce_result_count', 20 );
   remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
}
// REMOVE ALL TABS 

/**
 * Remove product page tabs
 */
add_filter( 'woocommerce_product_tabs', 'my_remove_all_product_tabs', 98 );
 
function my_remove_all_product_tabs( $tabs ) {
  unset( $tabs['description'] );        // Remove the description tab
  unset( $tabs['reviews'] );       // Remove the reviews tab
  unset( $tabs['additional_information'] );    // Remove the additional information tab
  return $tabs;
}
// REMOVE ADDD TO CART 
add_action( 'woocommerce_single_product_summary', 'hide_add_to_cart_button_variable_product', 1, 0 );
function hide_add_to_cart_button_variable_product() {

    // Removing add to cart button and quantities only
    remove_action( 'woocommerce_single_variation', 'woocommerce_single_variation_add_to_cart_button', 20 );
}
// HIDE SKU 
add_filter( 'wc_product_sku_enabled', '__return_false' );

// HIDE PRICE
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );

// HIDE CATEGORY 
/* Remove Categories from Single Products */ 
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

add_action('woocommerce_after_shop_loop_item','woo_new_product_tab_content');
function woo_new_product_tab_content() {
    global $product;

    $ingredients = $product->get_attributes( 'pakendi-suurus' );

    foreach( $ingredients as $attr_name => $attr ){
        foreach( $attr->get_terms() as $term ){
            if ( wc_attribute_label( $attr_name ) == "400ml" ) {
                echo $term->name ;
                $meta_image = get_wp_term_image($term->term_id);
                echo '<img src="'.$meta_image.'"/>';
            } 
            else echo '';
        }
    }
}
/**
 * Shop/archives: wrap the product image/thumbnail in a div.
 * 
 * The product image itself is hooked in at priority 10 using woocommerce_template_loop_product_thumbnail(),
 * so priority 9 and 11 are used to open and close the div.
 */
add_action( 'woocommerce_before_shop_loop_item_title', function(){
    echo '<div class="product-img">';
}, 9 );
add_action( 'woocommerce_before_shop_loop_item_title', function(){
    echo '</div>';
}, 11 );

/**
 * Change number of related products output
 */ 
function woo_related_products_limit() {
  global $product;
	
	$args['posts_per_page'] = 4;
	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args', 20 );
  function jk_related_products_args( $args ) {
	$args['posts_per_page'] = 4; // 4 related products
	$args['columns'] = 1; // arranged in 2 columns
	return $args;
}