<?php
// THUMBNAIL SUPPORT
add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );
// WOOCOMMERCE SUPPORT 
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
   add_theme_support( 'woocommerce' );
}                               
//
add_action('woocommerce_shop_loop_item_title','fun',10);
function fun()
{
   echo '<button class="btn minimal">Tootest lähemalt<i></i></button>';
}

// GRAVITY FORMS CUSTOM SEND BUTTON
add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
function form_submit_button( $button, $form ) {
    return "<button class='button gform_button' id='gform_submit_button_{$form['id']}'>Saada<i></i></button>";
}