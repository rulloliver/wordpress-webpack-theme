<?php

add_action('init', 'create_posttype');
function create_posttype() {
    // news post type 
    register_post_type(
        'news',
        array(
            'labels' => array(
                'name' => __('News'),
                'singular_name' => __('news')
            ),
            'public' => true,
            'has_archive' => true,
             'hierarchical'               => true,
            'show_in_rest' => true, // gutenberg support
            'supports' => array('editor'), // gutenberg  spport
            'rewrite' => array(
                'with_front' => false,
                'slug' => 'news'
            ),
            'supports' => array(
                'title',
                'thumbnail',
                'editor',
                'excerpt'
            )
        )
    );
} // init types 