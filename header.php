<?php
/**
 * The header for our theme
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	<title><?php wp_title(); ?></title>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-223864588-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-223864588-1');
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WFC6C9Z');</script>
<!-- End Google Tag Manager -->
</head>
<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WFC6C9Z"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="page" class="site">
	<header id="masthead" class="site-header">
        <div class="container">
            <div class="columns">
                <div class="logo column">
                    <a href="<?php echo get_home_url(); ?>" class="main-logo"><img width="122" height="106" src="<?php bloginfo('template_directory'); ?>/assets/logo.svg"></a>
                </div><!-- logo -->

                <div class="nav-col column is-flex">
                    <nav id="site-navigation" class="main-navigation is-flex">
                        
                        <div class="mobile-menu is-hidden-desktop">
                            <div class="button_container" id="toggle">
                                <span class="top"></span>
                                <span class="middle"></span>
                                <span class="bottom"></span>
                            </div>
                            <div class="overlay" id="overlay">
                                <div class="container">
                                    <div class="columns menu-cols">
                                        <div class="column">
                                            <?php wp_nav_menu( array('menu' => 'primary-menu') ); ?>
                                        </div>
                                        <div class="column">
                                            <?php wp_nav_menu( array('menu' => 'footer-2') ); ?>
                                        </div>
                                    </div>  
                                    <div class="overlay--details">
                                        <div class="overlay--social is-flex">
                                            <?php if(get_field('facebook','option') ) : ?>
                                                <a class="social-icon" href="<?php the_field('facebook','option'); ?>">
                                                    <span class="social-icon--holder fb"></span>
                                                </a>
                                            <?php endif; ?>
                                            <?php if(get_field('instagram','option') ) : ?>
                                                <a class="social-icon" href="<?php the_field('instagram','option'); ?>">
                                                    <span class="social-icon--holder ig"></span>
                                                </a>
                                            <?php endif; ?>
                                            <?php if(get_field('youtube','option') ) : ?>
                                                <a class="social-icon" href="<?php the_field('youtube','option'); ?>">
                                                    <span class="social-icon--holder yt"></span>
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                        <?php if(get_field('m-contact','option')): ?>
                                            <div class="overlay--copy">
                                                <?php the_field('m-contact','option'); ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="desktop-menu is-hidden-mobile">
                            <?php wp_nav_menu( array('menu' => 'primary-menu') ); ?>
                        </div><!-- desktop -->
                    </nav><!-- #site-navigation -->
                    <div id="search-btn" class="icon-search">
                        <span></span>
                    </div>
                       
                </div><!-- nav-col -->
                 <div class="search-wrap"><?php echo do_shortcode('[search_live inhibit_enter="yes" show_description="yes" placeholder="Otsi lehelt..." no_results="Kahjuks ei leitud ühtegi tulemust!" thumbnails="no" limit="8"]'); ?></div>
            </div><!-- columns -->
        
                  
         

	    </div><!-- container -->
    </header><!-- #masthead -->

<div id="content" class="site-content">
