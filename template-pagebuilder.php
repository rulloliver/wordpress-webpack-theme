<?php get_header(); 
    /* Template name: Page builder */
?>
<?php if( have_rows('page-builder') ): ?>
<div class="page-builder">

    <?php while( have_rows('page-builder') ): the_row(); ?>

		<?php if( get_row_layout() == 'hero-slider'): ?>

            <?php get_template_part('partials/hero','slider'); ?> 

        <?php elseif( get_row_layout() == 'hero'): ?>

            <?php get_template_part('partials/hero'); ?> 

        <?php elseif( get_row_layout() == 'banner'): ?>

            <?php get_template_part('partials/banner'); ?>

        <?php elseif( get_row_layout() == 'recent-products'): ?>

            <?php get_template_part('partials/recent','products'); ?>

        <?php elseif( get_row_layout() == 'recent-news'): ?>

            <?php get_template_part('partials/recent','news'); ?>

        <?php elseif( get_row_layout() == 'recent-posts'): ?>

            <?php get_template_part('partials/recent','posts'); ?>

        <?php elseif( get_row_layout() == 'full-video'): ?>

            <?php get_template_part('partials/videoplayer'); ?>

        <?php elseif( get_row_layout() == 'regular-content'): ?>

            <?php get_template_part('partials/regular','content'); ?>   

        <?php elseif( get_row_layout() == 'three-columns'): ?>

            <?php get_template_part('partials/three','columns'); ?>  

        <?php elseif( get_row_layout() == 'contact-details'): ?>

            <?php get_template_part('partials/contact','details'); ?>  

        <?php elseif( get_row_layout() == 'numblocks'): ?>

            <?php get_template_part('partials/numblocks'); ?>  

        <?php elseif( get_row_layout() == 'resellersblock'): ?>

            <?php get_template_part('partials/resellersblock'); ?>  

        <?php elseif( get_row_layout() == 'team'): ?>

            <?php get_template_part('partials/people'); ?>  

        <?php elseif( get_row_layout() == 'faq-hero'): ?>

            <?php get_template_part('partials/faq','hero'); ?>

        <?php endif; ?><!-- end layouts -->

    <?php endwhile; // pagebuilder ?> 
    
</div><!-- builder -->
<?php endif; ?>

<?php get_footer(); ?>