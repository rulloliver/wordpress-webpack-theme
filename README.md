# Wordpress Webpack + bulma.io theme

- Have Node.js and NPM / Yarn installed
- Run `npm i` to install npm packages
- Make any necessary changes to the config at scripts/webpack.config.js (theme path / browserSync details)
- and package.json scripts when using browser-sync with a proxy
- `npm start` to develop & `npm run build` when building for production
- Also includes bitbucket-pipelines.yml file: configure correct variables in repository

