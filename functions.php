<?php

// Global functions
require_once 'inc/global-functions.php';

// Theme Customizer
require_once 'inc/customizer.php';

// Widgets
require_once 'inc/widgets.php';

// Custom Post Types and Tax
require_once 'inc/custom-types.php';

// Actions
require_once 'inc/actions.php';

// Filters
require_once 'inc/filters.php';

// Shortcodes
require_once 'inc/shortcodes.php';

// AJAX endpoints
require_once 'inc/ajax.php';

// Woocommerce customization
require_once 'inc/woocommerce.php';

// ACF options / settings
require_once 'inc/acf-options.php';