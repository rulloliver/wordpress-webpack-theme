<form id="searchform" action="/" method="get">
    <input type="image" alt="Search" src="<?php bloginfo( 'template_url' ); ?>/assets/search_icon.svg" />
    <input type="text" name="s" id="search" value="<?php the_search_query(); ?>" placeholder="Search for a keyword..."/>
</form>